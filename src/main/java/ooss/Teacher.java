package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> teachingKlasses = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder stringBuilder = new StringBuilder(super.introduce());
        if (this.teachingKlasses.size() > 0) {
            stringBuilder.append(" I am a teacher. I teach Class ");
            String join = String.join(", ", this.teachingKlasses.stream().map(teachingKlass ->
                    String.valueOf(teachingKlass.getNumber())).collect(Collectors.toList())) + '.';
            return stringBuilder.append(join).toString();
        }
        return stringBuilder.append(" I am a teacher.").toString();
    }

    public void assignTo(Klass klass) {
        this.teachingKlasses.add(klass);
        klass.people.add(this);
    }

    public boolean belongsTo(Klass klass) {
        return teachingKlasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return teachingKlasses.contains(student.getKlass());
    }

    @Override
    protected void response(Object klass) {
        Klass _klass = (Klass) klass;
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", this.getName(), _klass.getNumber(), _klass.getLeader().getName()));
    }
}
