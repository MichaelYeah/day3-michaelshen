package ooss;

import java.util.Objects;

public class Person extends Observer {
    private int id;
    private String name;
    private int age;
    public String introduce() {
        return String.format("My name is %s. I am %d years old.", this.name, this.age);
    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    protected void response() {

    }

    @Override
    protected void response(Object object) {

    }
}
