package ooss;

public abstract class Observer {
    protected abstract void response();
    protected abstract void response(Object object);
}
