package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass extends Observed {
    private int number;
    private Student leader;
//    protected List<Student> students = new ArrayList<>();
//    protected List<Teacher> teachers = new ArrayList<>();
    protected List<Person> people = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            this.leader = student;
//            teachers.stream().forEach(teacher -> System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", teacher.getName(), this.getNumber(), student.getName())));
//            students.stream().forEach(stu -> System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", stu.getName(), this.getNumber(), student.getName())));
            notifyObserver();
        }
        else System.out.println("It is not one of us.");
    }

    public boolean isLeader(Student student) {
        return student.equals(this.leader);
    }

    public boolean attach(Teacher teacher) {
        return teacher.belongsTo(this);
    }

    public boolean attach(Student student) {
        return student.isIn(this);
    }

    @Override
    protected void notifyObserver() {
        people.stream().forEach(person -> {
            if (person instanceof Teacher) {
                ((Teacher) person).response(this);
            } else {
                ((Student) person).response();
            }
        });
    }

    public Student getLeader() {
        return leader;
    }
}
