package ooss;

public class Student extends Person {

    private Klass klass = null;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder stringBuilder = new StringBuilder(super.introduce());
        if (this.klass != null) {
            if (klass.getLeader() != null && klass.getLeader().equals(this)) stringBuilder.append(String.format(" I am a student. I am the leader of class %d.", this.klass.getNumber()));
            else stringBuilder.append(String.format(" I am a student. I am in class %d.", this.klass.getNumber()));
        }
        else
            stringBuilder.append(String.format(" I am a student."));
        return stringBuilder.toString();
    }

    public void join(Klass klass) {
        this.klass = klass;
        klass.people.add(this);
    }

    public boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    protected void response() {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", this.getName(), this.klass.getNumber(), this.klass.getLeader().getName()));
    }
}
