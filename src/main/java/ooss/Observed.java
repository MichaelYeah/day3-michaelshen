package ooss;

public abstract class Observed {
    protected abstract void notifyObserver();
}
