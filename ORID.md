Objective:
1.	We practiced doing code review and communicated with my teammates after that.  By code reviewing, I learned something useful from my teammates’ codes, found their problems as well as modifying my own shortcomings in my codes.
2.	I tried to use Stream API to replace the “for loop” as well as tried some IDEA keyboard shortcuts to improve development efficiency.
3.	I learned OOP and practiced to write codes of Car, Driver and so on with the idea of OOP.

Reflective:
1.	I feel content to learn from others and modify my own false.
2.	I am pretty happy that I am more and more familiar with Stream API after practicing them in the class.
3.	I found that the Java classes I coded have something wrong when I created more other classes and I feel so tired to modify them.

Interpretive:
1.	When reviewing one’s code, I need to interpret the map he draw and peruse his code in order to deeply understand the his software system. During this process, I compare our codes and learn new things, drawing on others’ strong points to offset one’s own weaknesses, making up for one’s deficiencies by learning from others’ strong points”.
2.	I am inexperienced in OOP so that I didn’t form a mature software architecture in my mind, for which I performed pool in designing a java class.

Decisional:
1.	I decide to complete my homework more seriously and reduce the simple false. I also want to evaluate my ability to read others’ source code, so that I can learn more from them in a limited time.
2.	I decided to get good hang of Stream API and replace all the for or while loop with them in my following coding career.
3.	I decided to learn more about software architecture as well as gaining experiences in the future projects.
